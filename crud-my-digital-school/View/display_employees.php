<?php 
  // print_r($data);
?>

<table class="table table-dark text-center my-5">
  <thead>
    <tr>
        <?php foreach ($fields as $column) : ?>
          <td><?= $column['Field'] ?></td>
        <?php endforeach; ?>
        <td>View</td>
        <td>Edit</td>
        <td>Delete</td>
      </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $employee) : ?>
      <tr>
        <td><?= implode('</td><td>', $employee) ?></td>
        <td><a href="?userChoice=select&id=<?= $employee["$id"] ?>" class="btn btn-success"><i class="bi bi-eye"></i></a></td>
        <td><a href="?userChoice=update&id=<?= $employee["$id"] ?>" class="btn btn-warning"><i class="bi bi-pencil"></i></a></td>
        <td><a href="?userChoice=delete" class="btn btn-danger"><i class="bi bi-trash"></i></a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>