<?php $userChoice = $_GET['userChoice'] ?>
<h2 class="text-center"><?= ($userChoice == "add") ? "Ajout d'employé" : "modification de profil" ?></h2>

<form class="col-3 my-5" method="POST" action="">
  <?php foreach($fields as $field): 
  if($field['Field'] != 'idEmploye'): ?>
   
    <div class="mb-3">
        <label class="form-label" for="<?= $field['Field'] ?>"><?= $field['Field'] ?></label><br>
        <input class="form-control" id="<?= $field['Field'] ?>"  name="<?= $field['Field'] ?>" type="text" value="<?= ($userChoice == "update") ? $field['Field'] : ''?>">
    </div>
    <?php endif; ?>
    <?php endforeach;?>
    <button class="btn btn-success" type="submit">Envoyer</button>
</form>