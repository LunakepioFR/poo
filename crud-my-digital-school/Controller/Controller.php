<?php

namespace Controller;

use Model;

class Controller
{
  private $dbEntityRepository;
  
  public function __construct()
  {
    $this->entityRepository = new Model\EntityRepository;
  }

  public function handleRequest()
  {
    $userChoice = $_GET['userChoice'] ?? NULL;

    try {
      switch ($userChoice) {
        case 'add':
          $this->save($userChoice);
          break;
        case 'update':
          return $this->save($userChoice);
          break;
        case 'select':
          return $this->select();
          break;
        case 'delete':
          return $this->delete();
          break;
        
        default:
          return $this->selectAll();
          break;
      }
    } catch (\Exception $err) {
      echo 'An error occurred during get form informations. \n';
      echo 'Error: ' . $err->getMessage() . '\n Line: ' . $err->getLine() . ' in ' . $err->getFile() . ' file';
    }
  }

  public function render($layout, $template, $parameters = array())
  {
    extract($parameters);
    ob_start();
    require ("view/$template");
    $content = ob_get_clean();

    ob_start();
    require ("View/$layout");

    return ob_end_flush();
  }

  public function selectAll()
  {
    $this->render('layout.php', 'display_employees.php',
      [
        'title' => 'Manage employees',
        'data' => $this->entityRepository->getAllEmployees(),
        'fields' => $this->entityRepository->getFields(),
        'id' => 'id' . ucfirst($this->entityRepository->table),
      ]
    );
  }
  
  public function save($userChoice)
  {
    $id = isset($_GET['id']) ? $_GET['id'] : NULL;
    $data = ($userChoice === 'update') ? $this->entityRepository->getFields() : NULL;
    if($_POST){
      $this->dbEntityRepository->saveEntityRepo($id);
      header('Location: index.php');
      exit;
    }
    
    $this->render('layout.php', 'contact_form.php',
      [
        'title' => 'Manage employees',
        'data' => $data,
        'fields' => $this->entityRepository->getFields(),
        'id' => 'id' . ucfirst($this->entityRepository->table),
      ]
    );

  
  }
  
  public function select()
  {
    $id = isset($_GET['id']) ? $_GET['id'] : NULL;

    $this->render('layout.php', 'employee.php',
      [
        'title' => 'Personnnal informations of ',
        'data' => $this->entityRepository->getEmployeeById($id),
      ]
    );
  }

  public function delete() : string
  {
    return "Delete an employee";
  }
}